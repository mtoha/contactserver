<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PhoneRepository
 *
 * @author mtoha
 */
class PhoneRepository  extends BaseRepository {

    public static function getAll() {
        return Phone::all();
    }

    public static function find($id) {
        return Phone::find($id);
    }

    public static function create($input) {
        return Phone::create($input);
    }

    public static function delete($id) {
        return Phone::find($id)->delete();
    }
    
    public static function update($input) {
        return $input->save();
    }
}
