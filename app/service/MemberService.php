<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MemberService
 *
 * @author mtoha
 */
class MemberService  extends BaseService {

    public static function getAll() {
        return MemberRepository::getAll();
    }

    public static function find($id) {
        return MemberRepository::find($id);
    }
    public static function findByUsername($username){
        return MemberRepository::findByUsername($username);
    }

    public static function create($input) {
        if ($input["password"] == $input["password_retype"]) {
            unset($input["password_retype"]);
            $input['password'] = Hash::make($input['password']);
            $object = array(
                "username" => $input["username"],
                "password" => $input['password'],
                "phone" => $input["phone"],
                "email" => $input["email"],
                "type" => $input["type"]
            );
            return MemberRepository::create($object);
        }
    }

    public static function delete($id) {
        return MemberRepository::delete($id);
    }

    public static function update($input) {
        $id = $input['id'];
        $object = MemberService::find($id);
        $object->username = $input["username"];
        $password_check = Hash::check($input["password_old"], $object->password);
        $password_valid = $input['password'] == $input["retype_password"];
        if ($password_check && $password_valid) {
            $object->username = Hash::make($input["username"]);
            $object->password = $input['password'];
            $object->phone = $input["phone"];
            $object->email = $input["email"];
            $object->type = $input["type"];
            return ContactRepository::update($object);
        } else {
            Session::flash('message', "Error : Please check Your password");
            return ContactServerMessages::$MESSAGE_WRONG_PASSWORD;
        }
    }
}
