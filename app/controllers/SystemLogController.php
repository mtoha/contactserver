<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SystemLogController
 *
 * @author mtoha
 */
class SystemLogController extends BaseController {

    public function postCreate() {
        $json = file_get_contents('php://input');
        $data = json_decode($json);
        
//        $validation = Validator::make($input, SystemLog::$rules);
//        if ($validation->passes()) {
//            $systemLog = SystemLogService::create($input);
//            return Response::json($systemLog);
//        }
//        Session::flash('message', "Error : Validation Error");
//        return ContactServerMessages::$MESSAGE_FAIL;
        
        $object = SystemLogService::create($data);
        return Response::json($object);
    }

    public function getView($id) {
        $object = SystemLogService::find($id);
        $data['systemLog'] = $object;
        return Response::json($data);
    }

    public function getEdit($id) {
        if (Auth::check()) {
            $object = SystemLogService::find($id);
            return Response::json($object);
        } else {
            return ContactServerMessages::$MESSAGE_NOT_AUTHENTICATED;
        }
    }

    public function postUpdate() {
        $input = Input::all();
        $validation = Validator::make($input, SystemLog::$rules);
        if ($validation->passes()) {
            $object = SystemLogService::update($input);
            return Response::json($object);
        }
        Session::flash('message', "Error : Validation Error");
        return ContactServerMessages::$MESSAGE_FAIL;
    }

    public function getDestroy($id) {
        if (Auth::check()) {
            $object = SystemLogService::destroy($id);
            return Response::json($object);
        } else {
            return ContactServerMessages::$MESSAGE_NOT_AUTHENTICATED;
        }
    }

    public function getIndex() {
        $objects = SystemLogService::getAll();
        $data["objects"] = $objects;
        return Response::json($data);
    }

}
