<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SystemLogRepository
 *
 * @author mtoha
 */
class SystemLogRepository  extends BaseRepository {

    public static function getAll() {
        return SystemLog::all();
    }

    public static function find($id) {
        return SystemLog::find($id);
    }

    public static function create($input) {
        return SystemLog::create($input)->id;
    }

    public static function delete($id) {
        return SystemLog::find($id)->delete();
    }
    
    public static function update($input) {
        return $input->save();
    }
}
