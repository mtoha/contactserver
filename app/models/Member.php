<?php

class Member extends Eloquent
{
    protected $table = 'members';
    public $timestamps = true;
    protected $softDelete = true;
    //Region Laravel Rules
    protected $guarded = array("id");
    public static $unguarded = true;
    //Region Rules
    public  static $rules = array(
        "username"  => "required",
        "phone" => "required",
        "email"=>"required"
    );


    public function contacts()
    {
        return $this->hasMany('Contact');
    }

}