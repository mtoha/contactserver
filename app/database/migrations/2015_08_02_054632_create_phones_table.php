<?php

use Illuminate\Database\Migrations\Migration;

class CreatePhonesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('phones', function($table) {
            $table->increments('id');
            $table->string('username', 50);
            $table->string('phoneNumber', 20);
            $table->integer('contact_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('phones');
    }

}