<?php

class Email extends Eloquent {

    protected $table = 'emails';
    public $timestamps = true;
    protected $softDelete = true;
    //Region Laravel Rules
    protected $guarded = array("id");
    public static $unguarded = true;
    //Region Rules
    public static $rules = array(
        "username" => "required",
        "email" => "required",
        "contact_id" => "required"
    );

    public function contact() {
        return $this->belongsTo('Contact');
    }

}
