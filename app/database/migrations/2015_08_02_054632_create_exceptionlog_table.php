<?php

use Illuminate\Database\Migrations\Migration;

class CreateExceptionlogTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exceptionlog', function($table) {
            $table->increments('id');
            $table->string('RefNumber', 200);
            $table->string('source', 250);
            $table->string('userName', 250);
            $table->text('Description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('exceptionlog');
    }

}