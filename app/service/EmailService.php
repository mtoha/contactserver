<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EmailService
 *
 * @author mtoha
 */
class EmailService extends BaseService {

    public static function getAll() {
        return EmailRepository::getAll();
    }

    public static function find($id) {
        return EmailRepository::find($id);
    }

    public static function create($input) {
        $object = array(
            "username" => $input["username"],
            "email" => $input['email'],
            "contact_id" => $input["contact_id"]
        );
        return EmailRepository::create($object);
    }

    public static function delete($id) {
        return EmailRepository::delete($id);
    }

    public static function update($input) {
        $id = $input['id'];
        $object = EmailService::find($id);
        $object->username = $input["username"];
        $object->email = $input["email"];
        $object->contact_id = $input["contact_id"];

        return EmailRepository::update($object);
    }
}
