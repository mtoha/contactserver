<?php

class SystemLog extends Eloquent {

    protected $table = 'systemlog';
    public $timestamps = true;
    //Region Laravel Rules
    protected $guarded = array("id");
    public static $unguarded = true;
    //Region Rules
    public static $rules = array(
        "refNumber" => "required",
        "source" => "required",
        "userName" => "required",
        "description" => "required"
    );

}
