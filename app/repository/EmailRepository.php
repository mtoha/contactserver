<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EmailRepository
 *
 * @author mtoha
 */
class EmailRepository  extends BaseRepository {

    public static function getAll() {
        return Email::all();
    }

    public static function find($id) {
        return Email::find($id);
    }

    public static function create($input) {
        return Email::create($input);
    }

    public static function delete($id) {
        return Email::find($id)->delete();
    }
    
    public static function update($input) {
        return $input->save();
    }
}
