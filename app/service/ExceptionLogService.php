<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ExceptionLogService
 *
 * @author mtoha
 */
class ExceptionLogService extends BaseService {

    public static function getAll() {
        return ExceptionLogRepository::getAll();
    }

    public static function find($id) {
        return ExceptionLogRepository::find($id);
    }

    public static function create($data) {
        $object = array(
            "refNumber" => $data->refNumber,
            "source" => $data->source,
            "userName" => $data->userName,
            "description" => $data->description
        );
        ExceptionLogRepository::create($object);
        return $object;
    }

    public static function delete($id) {
        return ExceptionLogRepository::delete($id);
    }

    public static function update($input) {
        $id = $input['id'];
        $object = ExceptionLogService::find($id);
        $object->refNumber = $input["refNumber"];
        $object->source = $input["source"];
        $object->userName = $input["userName"];
        $object->description = $input["description"];
        
        return ExceptionLogRepository::update($object);
    }

}
