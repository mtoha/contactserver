<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ContactRepository
 *
 * @author mtoha
 */
class ContactRepository  extends BaseRepository {

    public static function getAll() {
        return Contact::all();
    }

    public static function find($id) {
        return Contact::find($id);
    }

    public static function create($input) {
        return Contact::create($input);
    }

    public static function delete($id) {
        return Contact::find($id)->delete();
    }
    
    public static function update($input) {
        return $input->save();
    }

}
?>
