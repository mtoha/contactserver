<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MemberRepository
 *
 * @author mtoha
 */
class MemberRepository  extends BaseRepository {

    public static function getAll() {
        return Member::all();
    }

    public static function find($id) {
        return Member::find($id);
    }
    
public static function findByUsername($username) {
        return Member::where('username','=', $username)->firstOrFail();
    }
    public static function create($input) {
        return Member::create($input);
    }

    public static function delete($id) {
        return Member::find($id)->delete();
    }
    
    public static function update($input) {
        return $input->save();
    }
}
