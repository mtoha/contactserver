<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MemberController
 *
 * @author mtoha
 */
class MemberController extends BaseController {
     public function postCreate() {
        $input = Input::only("username", "password", "password_retype","phone", "email", "type");
        $validation = Validator::make($input, Member::$rules);
        if ($validation->passes()) {
            $object = MemberService::create($input);
            return Response::json($object);
        }
        Session::flash('message', "Error : Validation Error");
        return ContactServerMessages::$MESSAGE_FAIL;
    }

    public function getView($id) {
        $object = MemberService::find($id);
        $data['member'] = $object;
        return Response::json($data);
    }

    public function getEdit($id) {
        if (Auth::check()) {
            $object = PhoneService::find($id);
            if (is_null($object)) {
                return ContactServerMessages::$MESSAGE_FAIL;
            }
            $data['member'] = $object;
            return Response::json($data);
        } else {
            return ContactServerMessages::$MESSAGE_NOT_AUTHENTICATED;
        }
    }

    public function postUpdate() {
        $input = Input::all();
        $validation = Validator::make($input, Member::$rules);
        if ($validation->passes()) {
            $object = MemberService::update($input);
            return Response::json($object);
        }
        Session::flash('message', "Error : Validation Error");
        return ContactServerMessages::$MESSAGE_FAIL;
    }

    public function getDestroy($id) {
        if (Auth::check()) {
            $object = MemberService::destroy($id);
            return Response::json($object);
        } else {
            return ContactServerMessages::$MESSAGE_NOT_AUTHENTICATED;
        }
    }

    public function getIndex() {
        $objects = MemberService::getAll();
        $data["members"] = $objects;
        return Response::json($data);
    }
}
