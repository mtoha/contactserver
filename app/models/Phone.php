<?php

class Phone extends Eloquent {

    protected $table = 'phones';
    public $timestamps = true;
    protected $softDelete = true;
    //Region Laravel Rule
    protected $guarded = array("id");
    public static $unguarded = true;
    //Region Rules
    public static $rules = array(
        "username" => "required",
        "phoneNumber" => "required",
        "contact_id" => "required"
    );

    public function contact() {
        return $this->belongsTo('Contact');
    }

}
