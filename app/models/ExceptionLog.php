<?php

class ExceptionLog extends Eloquent
{
    protected $table = 'exceptionlog';
    public $timestamps = true;
    //Region Laravel Rule
    protected $guarded = array("id");
    public static $unguarded = true;
    //Region Rules
    public static $rules = array(
        "RefNumber" => "required",
        "source" => "required",
        "userName" => "required",
        "Description" => "required"
    );

}