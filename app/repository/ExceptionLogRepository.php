<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ExceptionLogRepository
 *
 * @author mtoha
 */
class ExceptionLogRepository  extends BaseRepository {

    public static function getAll() {
        return ExceptionLog::all();
    }

    public static function find($id) {
        return ExceptionLog::find($id);
    }

    public static function create($input) {
        return ExceptionLog::create($input);
    }

    public static function delete($id) {
        return ExceptionLog::find($id)->delete();
    }
    
    public static function update($input) {
        return $input->save();
    }
}
