<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ContactService
 *
 * @author mtoha
 */
class ContactService extends BaseService {

    public static function getAll() {
        return ContactRepository::getAll();
    }

    public static function find($id) {
        return ContactRepository::find($id);
    }

    public static function create($input) {
        $member = MemberService::findByUsername($input->username);
        $contact = array(
            "username" => $input->username,
            "contactName" => $input->contactName,
            "detail" => $input->detail,
            "member_id" => $member->id
        );
        ContactRepository::create($contact);
        return $contact;
    }

    public static function delete($id) {
        return ContactRepository::delete($id);
    }

    public static function update($input) {
        $id = $input['id'];
        $object = ContactService::find($id);
        $object->username = $input["username"];
        $object->contactName = $input["contactName"];
        $object->detail = $input["detail"];
        $object->member_id = $input["member_id"];

        return ContactRepository::update($object);
    }
}
