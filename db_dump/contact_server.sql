-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 21, 2016 at 08:16 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `contact_server`
--

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE IF NOT EXISTS `contacts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `contactName` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `detail` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `member_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `emails`
--

CREATE TABLE IF NOT EXISTS `emails` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `contact_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exceptionlog`
--

CREATE TABLE IF NOT EXISTS `exceptionlog` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `RefNumber` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `source` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `userName` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `Description` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `exceptionlog`
--

INSERT INTO `exceptionlog` (`id`, `RefNumber`, `source`, `userName`, `Description`, `created_at`, `updated_at`) VALUES
(1, 'Sat Aug 15 08:26:11 GMT+07:00 2015', 'class com.essential.toha.tohaandroidessentialtraining.MainActivity', 'mtoha', 'test', '2015-08-14 18:26:35', '2015-08-14 18:26:35'),
(2, '20150815091219', 'class com.essential.toha.tohaandroidessentialtraining.MainActivity', 'mtoha', 'test', '2015-08-14 19:12:33', '2015-08-14 19:12:33'),
(3, '20150815190458', 'class com.essential.toha.tohaandroidessentialtraining.MainActivity', 'mtoha', 'test', '2015-08-15 05:05:51', '2015-08-15 05:05:51'),
(4, '20150816230327', 'class com.essential.toha.tohaandroidessentialtraining.MainActivity', 'mtoha', 'test', '2015-08-16 09:03:47', '2015-08-16 09:03:47'),
(5, '20150816230418', 'class com.essential.toha.tohaandroidessentialtraining.MainActivity', 'mtoha', 'test', '2015-08-16 09:04:36', '2015-08-16 09:04:36');

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE IF NOT EXISTS `members` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `type` enum('admin','moderator','user') COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2015_08_02_054632_create_contacts_table', 1),
('2015_08_02_054632_create_emails_table', 1),
('2015_08_02_054632_create_exceptionlog_table', 1),
('2015_08_02_054632_create_members_table', 1),
('2015_08_02_054632_create_phones_table', 1),
('2015_08_02_054632_create_systemlog_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `phones`
--

CREATE TABLE IF NOT EXISTS `phones` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `phoneNumber` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `contact_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `systemlog`
--

CREATE TABLE IF NOT EXISTS `systemlog` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `RefNumber` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `source` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `userName` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `Description` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `systemlog`
--

INSERT INTO `systemlog` (`id`, `RefNumber`, `source`, `userName`, `Description`, `created_at`, `updated_at`) VALUES
(1, 'Sat Aug 15 08:28:46 GMT+07:00 2015', 'class com.essential.toha.tohaandroidessentialtraining.MainActivity', 'mtoha', 'test', '2015-08-14 18:29:05', '2015-08-14 18:29:05'),
(2, '20150815190556', 'class com.essential.toha.tohaandroidessentialtraining.MainActivity', 'mtoha', 'test', '2015-08-15 05:06:04', '2015-08-15 05:06:04'),
(3, '20150815214711', 'class com.essential.toha.tohaandroidessentialtraining.MainActivity', 'mtoha', 'test', '2015-08-15 07:47:50', '2015-08-15 07:47:50'),
(4, '20150815214824', 'class com.essential.toha.tohaandroidessentialtraining.MainActivity', 'mtoha', 'test', '2015-08-15 07:48:49', '2015-08-15 07:48:49'),
(5, '20150815215431', 'class com.essential.toha.tohaandroidessentialtraining.MainActivity', 'mtoha', 'test', '2015-08-15 07:54:55', '2015-08-15 07:54:55'),
(6, '20150816073022', 'class com.essential.toha.tohaandroidessentialtraining.MainActivity', 'mtoha', 'test', '2015-08-15 17:30:26', '2015-08-15 17:30:26'),
(7, '20150816073045', 'class com.essential.toha.tohaandroidessentialtraining.MainActivity', 'mtoha', 'test', '2015-08-15 17:30:49', '2015-08-15 17:30:49'),
(8, '20150817094642', 'class com.essential.toha.tohaandroidessentialtraining.MainActivity', 'mtoha', 'test', '2015-08-16 19:46:54', '2015-08-16 19:46:54');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
