<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PhoneController
 *
 * @author mtoha
 */
class PhoneController extends BaseController {

    public function postCreate() {
        $input = Input::only("username", "phoneNumber", "contact_id");
        $validation = Validator::make($input, Phone::$rules);
        if ($validation->passes()) {
            $object = PhoneService::create($input);
            return Response::json($object);
        }
        Session::flash('message', "Error : Validation Error");
        return ContactServerMessages::$MESSAGE_FAIL;
    }

    public function getView($id) {
        $object = PhoneService::find($id);
        $data['phone'] = $object;
        return Response::json($data);
    }

    public function getEdit($id) {
        if (Auth::check()) {
            $object = PhoneService::find($id);
            if (is_null($object)) {
                return View::make("admin/index");
            }
            $data['phone'] = $object;
            return Response::json($data);
        } else {
            return ContactServerMessages::$MESSAGE_NOT_AUTHENTICATED;
        }
    }

    public function postUpdate() {
        $input = Input::all();
        $validation = Validator::make($input, Phone::$rules);
        if ($validation->passes()) {
            $object = PhoneService::update($input);
            return Response::json($object);
        }
        Session::flash('message', "Error : Validation Error");
        return ContactServerMessages::$MESSAGE_FAIL;
    }

    public function getDestroy($id) {
        if (Auth::check()) {
            $object = PhoneService::destroy($id);
            return Response::json($object);
        } else {
            return ContactServerMessages::$MESSAGE_NOT_AUTHENTICATED;
        }
    }

    public function getIndex() {
        $objects = PhoneService::getAll();
        $data["objects"] = $objects;
        return Response::json($data);
    }
}
