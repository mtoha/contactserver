<?php

use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function($table) {
            $table->increments('id');
            $table->string('username', 50);
            $table->string('password', 64);
            $table->string('phone', 20);
            $table->string('email', 250);
            $table->enum('type', array("admin", "moderator", "user"));
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('members');
    }

}