<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SystemLogService
 *
 * @author mtoha
 */
class SystemLogService  extends BaseService {

    public static function getAll() {
        return SystemLogRepository::getAll();
    }

    public static function find($id) {
        return SystemLogRepository::find($id);
    }

    public static function create($data) {
        $object = array(
            "refNumber" => $data->refNumber,
            "source" => $data->source,
            "userName" => $data->userName,
            "description" => $data->description
        );
        $id = SystemLogRepository::create($object);
        $object["id"] = $id;
        return $object;
    }

    public static function delete($id) {
        return SystemLogRepository::delete($id);
    }
}
