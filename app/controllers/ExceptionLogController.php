<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ExceptionLogController
 *
 * @author mtoha
 */
class ExceptionLogController extends BaseController {

    public function postCreate() {
        $json = $_SERVER['HTTP_JSON'];
        $data = json_decode($json);
//        $input = Input::only("refNumber", "source", "userName", "description");
//        $validation = Validator::make($input, ExceptionLog::$rules);
//        if ($validation->passes()) {
//            $object = ExceptionLogService::create($input);
//            return Response::json($object);
//        }
        
//        Session::flash('message', "Error : Validation Error");
//        return ContactServerMessages::$MESSAGE_FAIL;
        
        $object = ExceptionLogService::create($data);
        return Response::json($object);
        
    }

    public function getView($id) {
        $object = ExceptionLogService::find($id);
        $data['exception_log'] = $object;
        return Response::json($data);
    }

    public function getEdit($id) {
        if (Auth::check()) {
            $object = ExceptionLogService::find($id);
            if (is_null($object)) {
                return ContactServerMessages::$MESSAGE_FAIL;
            }
            $data['exception_log'] = $object;
            return Response::json($data);
        } else {
            ContactServerMessages::$MESSAGE_NOT_AUTHENTICATED;
        }
    }

    public function postUpdate() {
        $input = Input::all();
        $validation = Validator::make($input, ExceptionLog::$rules);
        if ($validation->passes()) {
            $object = ExceptionLogService::update($input);
            return Response::json($object);
        }
        Session::flash('message', "Error : Validation Error");
        return ContactServerMessages::$MESSAGE_FAIL;
    }

    public function getDestroy($id) {
        if (Auth::check()) {
            $object = ExceptionLogService::destroy($id);
            return Response::json($object);
        } else {
            return ContactServerMessages::$MESSAGE_NOT_AUTHENTICATED;
        }
    }

    public function getIndex() {
        $objects = ExceptionLogService::getAll();
        $data["exception_logs"] = $objects;
        return Response::json($data);
    }

}
