<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PhoneService
 *
 * @author mtoha
 */
class PhoneService extends BaseService {

    public static function getAll() {
        return PhoneRepository::getAll();
    }

    public static function find($id) {
        return PhoneRepository::find($id);
    }

    public static function create($input) {
        $object = array(
            "username" => $input["username"],
            "phoneNumber" => $input['phoneNumber'],
            "contact_id" => $input["contact_id"]
        );
        return PhoneRepository::create($object);
    }

    public static function delete($id) {
        return PhoneRepository::delete($id);
    }

    public static function update($input) {
        $id = $input['id'];
        $object = PhoneService::find($id);
        $object->username = $input["username"];
        $object->phoneNumber = $input['phoneNumber'];
        $object->contact_id = $input["contact_id"];
        return PhoneRepository::update($object);
    }

}
