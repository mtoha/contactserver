<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ContactController
 *
 * @author mtoha
 */
class ContactController extends BaseController {

    public function postCreate() {
        $json = $_SERVER['HTTP_JSON'];
        $data = json_decode($json);
        $object = ContactService::create($data);
        return Response::json($object);
//        $json = $_SERVER['HTTP_JSON'];
//        $input = Input::only("username", "contactName", "detail", "member_id");
//        $validation = Validator::make($input, Contact::$rules);
//        if ($validation->passes()) {
//            $object = ContactService::create($input);
//            return Response::json($object);
//        }
//        Session::flash('message', "Error : Validation Error");
//        return ContactServerMessages::$MESSAGE_NOT_AUTHENTICATED;
    }

    public function getView($id) {
        $object = ContactService::find($id);
        $data['contact'] = $object;
        return Response::json($data);
    }

    public function getEdit($id) {
        if (Auth::check()) {
            $object = ContactService::find($id);
            if (is_null($object)) {
                return ContactServerMessages::$MESSAGE_FAIL;
            }
            $data['contact'] = $object;
            return Response::json($data);
        } else {
            return ContactServerMessages::$MESSAGE_NOT_AUTHENTICATED;
        }
    }

    public function postUpdate() {
        $input = Input::all();
        $validation = Validator::make($input, Contact::$rules);
        if ($validation->passes()) {
            $object = ContactService::update($input);
            return Response::json($object);
        }
        Session::flash('message', "Error : Validation Error");
        return ContactServerMessages::$MESSAGE_FAIL;
    }

    public function getDestroy($id) {
        if (Auth::check()) {
            $object = ContactService::destroy($id);
            return Response::json($object);
        } else {
            return ContactServerMessages::$MESSAGE_NOT_AUTHENTICATED;
        }
    }

    public function getIndex() {
        $objects = ContactService::getAll();
        $data["contacts"] = $objects;
        return Response::json($data);
    }
}
