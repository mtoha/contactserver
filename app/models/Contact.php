<?php

class Contact extends Eloquent
{
    protected $table = 'contacts';
    public $timestamps = true;
    protected $softDelete = true;
    //Region Laravel Rules
    protected $guarded = array("id");
    public static $unguarded = true;
    //Region Rules
    public static $rules = array(
        "username" => "required",
        "contactName" => "required",
        "detail" => "required",
        "member_id" => "required"
    );

    public function member()
    {
        return $this->belongsTo('Member');
    }

    public function phones()
    {
        return $this->hasMany('Phone');
    }

    public function emails()
    {
        return $this->hasMany('Email');
    }

}