<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EmailController
 *
 * @author mtoha
 */
class EmailController extends BaseController {

    public function postCreate() {
        $input = Input::only("username", "email", "contact_id");
        $validation = Validator::make($input, Email::$rules);
        if ($validation->passes()) {
            $admin = AdminService::create($input);
            return Response::json($admin);
        }
        Session::flash('message', "Error : Validation Error");
        return ContactServerMessages::$MESSAGE_FAIL;
    }

    public function getView($id) {
        $object = EmailService::find($id);
        $data['email'] = $object;
        return Response::json($data);
    }

    public function getEdit($id) {
        if (Auth::check()) {
            $object = EmailService::find($id);
            if (is_null($object)) {
                return ContactServerMessages::$MESSAGE_FAIL;
            }
            $data['email'] = $object;
            return Response::json($data);
        } else {
            return ContactServerMessages::$MESSAGE_NOT_AUTHENTICATED;
        }
    }

    public function postUpdate() {
        $input = Input::all();
        $validation = Validator::make($input, Email::$rules);
        if ($validation->passes()) {
            $object = EmailService::update($input);
            return Response::json($object);
        }
        Session::flash('message', "Error : Validation Error");
        return ContactServerMessages::$MESSAGE_NOT_AUTHENTICATED;
    }

    public function getDestroy($id) {
        if (Auth::check()) {
            $object = EmailService::destroy($id);
            return Response::json($object);
        } else {
            return ContactServerMessages::$MESSAGE_NOT_AUTHENTICATED;
        }
    }

    public function getIndex() {
        $objects = EmailService::getAll();
        $data["emails"] = $objects;
        return Response::json($data);
    }
}
